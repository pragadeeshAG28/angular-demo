import { Component, OnInit } from '@angular/core';
import { ViewEmp } from '../ViewEmp';

@Component({
  selector: 'app-view-emp',
  templateUrl: './view-emp.component.html',
  styleUrls: ['./view-emp.component.css']
})
export class ViewEmpComponent implements OnInit {

  pageHeader: string = 'Employee Details..';
  employeeId: number = 10;
  Name: string = 'pragadeesh';
  salary: number = 24000;
  DepartmentId: number = 2 ;
  DepartmentName: string =  "payroll" ;

  constructor() { }

  ngOnInit(): void {
  }

  empl: ViewEmp ={
    skill:[
      {skill_id:1,skill_name:"HTML"},
      {skill_id:2,skill_name:"CSS"},
      {skill_id:3,skill_name:"J2EE"},
    ]
    }
    }
