import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FavouritemovieComponent } from './favouritemovie/favouritemovie.component';
import { EmployeedetailsComponent } from './employeedetails/employeedetails.component';
import { QuantityIncrementComponent } from './quantity-increment/quantity-increment.component';
import { ViewEmpComponent } from './view-emp/view-emp.component';
import { EditEmpComponent } from './edit-emp/edit-emp.component';
import { EmployeeComponent } from './employee/employee.component';
import { RegSuccessComponent } from './reg-success/reg-success.component';

@NgModule({
  declarations: [
    AppComponent,
    FavouritemovieComponent,
    EmployeedetailsComponent,
    QuantityIncrementComponent,
    ViewEmpComponent,
    EditEmpComponent,
    EmployeeComponent,
    RegSuccessComponent
  ],  
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  
  
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
             