import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quantity-increment',
  templateUrl: './quantity-increment.component.html',
  styleUrls: ['./quantity-increment.component.css']
})
export class QuantityIncrementComponent implements OnInit {


   data:String;
  constructor(private router:Router) { }

  
  ngOnInit(): void {
  }
  onClick():void{
    this.data="Button is clicked";
  }

}
