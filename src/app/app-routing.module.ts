import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewEmpComponent } from './view-emp/view-emp.component';
import { QuantityIncrementComponent } from './quantity-increment/quantity-increment.component';
import { EditEmpComponent } from './edit-emp/edit-emp.component';
import { EmployeeComponent } from './employee/employee.component';
import { RegSuccessComponent } from './reg-success/reg-success.component';

const routes: Routes = [
  {path:'viewEmployee',component:ViewEmpComponent},
  {path:'editEmployee',component:EditEmpComponent},
  {path:'quantityIncrement',component:QuantityIncrementComponent},
  {path:'regsuccess',component: RegSuccessComponent},
  { path:'employee',component: EmployeeComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
