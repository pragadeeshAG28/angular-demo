
export class User {


    constructor(

    public name: string,
    public salary: number,
    public department: string,
    public skill_name: string,
    public skill_id: number,
    public subscribe:boolean

    )
    {}

}