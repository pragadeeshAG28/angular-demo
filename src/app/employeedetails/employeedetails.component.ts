import { Component, OnInit } from '@angular/core';
import { Employeedetails } from '../Employeedetails';



@Component({
  selector: 'app-employeedetails',
  templateUrl:'./employeedetails.component.html',
  styleUrls:  ['./employeedetails.component.css']
})
export class EmployeedetailsComponent implements OnInit {


  public id =3;
  public name="pragadeesh";
  public salary="2000";
  public department=1;
  public dept_name="payroll"; 

  constructor() {
   }

  ngOnInit(): void {
  }
  emp:Employeedetails={
    skill:[
        {skill_id:10,skill_name:"Java"},
        {skill_id:11,skill_name:"c++"},
        {skill_id:12,skill_name:"J2EE"},
      ]
    }
  }
