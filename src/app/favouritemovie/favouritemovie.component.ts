import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favouritemovie',
  template : `<h3>
                 Favourite movie:  {{moviename}}
              </h3>
              `,
  styles: []
})
export class FavouritemovieComponent implements OnInit {

  public moviename ="Lord of the Rings"
  constructor() { }

  ngOnInit(): void {
  }

}
